package katas.fizzbuzz;

import org.apache.commons.lang3.StringUtils;

public class Solution {
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZBUZZ = "Fizzbuzz";

    public static String fizzbuzz() {
        String[] elements = new String[100];
        for (int index = 0; index < 100; ++index) {
            elements[index] = evaluate(index + 1);
        }
        return StringUtils.join(elements, ",");
    }

    private static String evaluate(int value) {
        String result;
        if (value % 3 == 0 && value % 5 != 0) {
            result = FIZZ;
        } else if (value % 5 == 0 && value % 3 != 0) {
            result = BUZZ;
        } else if (value % 15 == 0) {
            result = FIZZBUZZ;
        } else {
            result = String.format("%d", value);
        }
        return result;
    }
}
