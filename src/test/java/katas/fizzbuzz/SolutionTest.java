package katas.fizzbuzz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class SolutionTest {
    private String[] elements;
    private static final String NUMBER_REGEX = "[0-9]";
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZBUZZ = "Fizzbuzz";
    @Before
    public void setup() {
        elements = Solution.fizzbuzz().split(",");
    }
    @Test
    public void itReturnsACommaSeparatedListOfOneHundredElements() {
        Assert.assertThat(elements.length, is(equalTo(100)));
    }
    @Test
    public void itReturnsElementsInAscendingOrder() {
        int lastItem = Integer.MIN_VALUE;
        for (String item : elements) {
            if (isNumber(item)) {
                int itemValue = Integer.parseInt(item);
                Assert.assertThat(itemValue > lastItem, is(true));
                lastItem = itemValue;
            }
        }
        Assert.assertThat(elements.length, is(equalTo(100)));
    }
    @Test
    public void itReturnsAListOfOneToOneHundred() {
        Assert.assertThat(elements[0], is(equalTo("1")));
        Assert.assertThat(elements[97], is(equalTo("98")));
        Assert.assertThat(elements[98], is(equalTo(FIZZ)));
        Assert.assertThat(elements[99], is(equalTo(BUZZ)));
    }
    @Test
    public void multiplesOfThreeAreFizzInsteadOfANumber() {
        for (int index=0; index<100; ++index) {
            if ((index+1) % 3 == 0 && (index+1) % 5 != 0) {
                Assert.assertThat(isFizz(elements[index]), is(true));
            } else {
                Assert.assertThat(isFizz(elements[index]), is(false));
            }
        }
    }
    @Test
    public void multiplesOfFiveAreBuzzInsteadOfANumber() {
        for (int index=0; index<100; ++index) {
            if ((index+1) % 5 == 0 && (index+1) % 3 != 0) {
                Assert.assertThat(isBuzz(elements[index]), is(true));
            } else {
                Assert.assertThat(isBuzz(elements[index]), is(false));
            }
        }
    }
    @Test
    public void multiplesOfThreeAndFiveAreFizzbuzzInsteadOfANumber() {
        for (int index=0; index<100; ++index) {
            if ((index+1) % 15 == 0) {
                Assert.assertThat(isFizzbuzz(elements[index]), is(true));
            } else {
                Assert.assertThat(isFizzbuzz(elements[index]), is(false));
            }
        }
    }
    private boolean isFizz(String value) {
        return FIZZ.equals(value);
    }
    private boolean isBuzz(String value) {
        return BUZZ.equals(value);
    }
    private boolean isFizzbuzz(String value) {
        return FIZZBUZZ.equals(value);
    }
    private boolean isNumber(String value) {
        Pattern regexPattern = Pattern.compile(NUMBER_REGEX);
        Matcher matcher = regexPattern.matcher(value);
        return matcher.find();
    }
}
