# FizzBuzz Kata #

## Implementation Example ##
This repository contains an example implementation of 
the FizzBuzz Kata following TDD.

The implementation is in Java and utilises Maven for 
dependency and build management.